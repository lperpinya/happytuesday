package com.example.helloworld2020;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Calendar;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        String hello = getString(R.string.hello);
        String nombreApp = getString (R.string.app_name);
        String happy = getString (R.string.happy);
        String message;


        String[] llistaDies = getResources().getStringArray(R.array.days_of_week);
        String[] llistaMesos= getResources().getStringArray(R.array.months);

        int dayOfWeek = Calendar.getInstance().get(Calendar.DAY_OF_WEEK);
        int month = Calendar.getInstance().get(Calendar.MONTH);

        // Sunday is 1, Monday is 2, Tuesday is 3.....
        dayOfWeek--;
        message = happy + " " + llistaDies[dayOfWeek];
        Toast.makeText (this,  message , Toast.LENGTH_LONG).show();

        // getString(R.string.xxx), getResources
        // findViewById
        TextView tvSaludo = findViewById (R.id.tvsaludo);
        tvSaludo.setText (message);

        TextView tvMonth = findViewById (R.id.tvmonth);
        tvMonth.setText (llistaMesos[month]);





        /* Modifiquem el codi perquè el Toast, mostri Feliç dimarts, Feliç dimecres */







    }
}
